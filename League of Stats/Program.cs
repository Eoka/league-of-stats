﻿using LeagueOfStats.Views;
using LeagueOfStats.AppController;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using LeagueOfStats.Controllers;

namespace LeagueOfStats
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            SetupApplication();
            try { Application.Run(SetupControllers()); }
            catch (Exception e) { Application.Exit(); }
        }

        private static Form SetupControllers()
        {
            var appController = new ApplicationController();

            var mainWindow = new MainWindow();
            var mainWindowController = new MainWindowController() { Window = mainWindow };
            mainWindow.selectedChampionChanged += mainWindowController.OnSelectedChampionChanged;
            mainWindow.searchChanged += mainWindowController.OnSearchTextChanged;
            mainWindow.itemInListClicked += mainWindowController.OnItemInListClicked;
            mainWindow.resetButtonClicked += mainWindowController.OnResetButtonClicked;
            mainWindow.item1Clicked += mainWindowController.OnItemBuildClick;
            mainWindow.item2Clicked += mainWindowController.OnItemBuildClick;
            mainWindow.item3Clicked += mainWindowController.OnItemBuildClick;
            mainWindow.item4Clicked += mainWindowController.OnItemBuildClick;
            mainWindow.item5Clicked += mainWindowController.OnItemBuildClick;
            mainWindow.item6Clicked += mainWindowController.OnItemBuildClick;

            try { mainWindowController.InitWindow(); }
            catch (Exception e) { throw e; }

            return mainWindow;
        }

        private static void SetupApplication()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
        }
    }
}
