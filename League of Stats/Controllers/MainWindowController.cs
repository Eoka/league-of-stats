﻿using System;
using System.Collections.Generic;
using System.Linq;
using League_of_Stats.Views.Interfaces;
using League_of_Stats.Models;
using RiotApi.Net.RestClient.Dto.LolStaticData.Champion;
using RiotApi.Net.RestClient.Dto.LolStaticData.Item;

namespace LeagueOfStats.Controllers
{
    public class MainWindowController
    {
        public IMainWindow Window { get; set; }

        private DataController data;
        private Build build;
        
        public void InitWindow()
        {
            try
            {
                data = new DataController();
            }
            catch (Exception e)
            {
                Window.ShowError("Couldn't initialize the data correctly. \nPlease check your connection and try to relaunch the program");
                throw e;
            }
            build = new Build();
            Window.ChampionsName = data.ChampionsName;
            Window.ItemsName = data.ItemsName;
        }

        public void OnSelectedChampionChanged(object sender, NameArgs e)
        {
            ChampionDto champion = data.GetChampionDto(e.Name);

            Window.ChampionImageURL = data.GetChampionImageURL(champion.Image.Full);
            Window.Title = champion.Title;
            Window.Lore = champion.Lore;

            Window.Health = champion.Stats.Hp.ToString("0.");
            Window.HealthRegen = champion.Stats.Hpregen.ToString("0.0") + "/5s";
            Window.Mana = champion.Stats.Mp.ToString("0.");
            Window.ManaRegen = champion.Stats.Mpregen.ToString("0.0") + "/5s";
            Window.Armor = champion.Stats.Armor.ToString("0.");
            Window.MagicResist = champion.Stats.Spellblock.ToString("0.");
            Window.MovementSpeed = champion.Stats.Movespeed.ToString("0.");
            Window.AttackDamage = champion.Stats.Attackdamage.ToString("0.");
            Window.AttackSpeed = (champion.Stats.Attackspeedoffset + 0.625).ToString("0.0");
            Window.CriticalChance = (champion.Stats.Crit * 100).ToString("0.") + "%";
            Window.Lifesteal = "0";
            Window.AbilityPower = "0";
            Window.CooldownReduction = "0";
            Window.Spellvamp = "0";
        }

        public void OnSearchTextChanged(object sender, EventArgs e)
        {
            var list = new List<string>();

            Window.ItemsName = data.ItemsName.Where(s => s.ToLower().Contains(Window.SearchText.ToLower())).ToList();
        }

        public void OnItemInListClicked(object sender, NameArgs e)
        {
            ItemDto item = data.GetItemDto(e.Name);
            build.AddItemToBuild(item);

            UpdateBuildWindow();
        }

        public void OnResetButtonClicked(object sender, EventArgs e)
        {
            build.Reset();
            UpdateBuildWindow();
        }

        public void OnItemBuildClick(object sender, NameArgs e)
        {
            build.RemoveItemAtIndex(int.Parse(e.Name));
            UpdateBuildWindow();
        }

        private void UpdateBuildWindow()
        {
            Window.Item1Image = build[0] != null ? data.GetItemImageUrl(build[0].Image.Full) : null;
            Window.Item2Image = build[1] != null ? data.GetItemImageUrl(build[1].Image.Full) : null;
            Window.Item3Image = build[2] != null ? data.GetItemImageUrl(build[2].Image.Full) : null;
            Window.Item4Image = build[3] != null ? data.GetItemImageUrl(build[3].Image.Full) : null;
            Window.Item5Image = build[4] != null ? data.GetItemImageUrl(build[4].Image.Full) : null;
            Window.Item6Image = build[5] != null ? data.GetItemImageUrl(build[5].Image.Full) : null;

            Window.Price = build.Price.ToString();

            UpdateChampionBonusStats();
        }

        private void UpdateChampionBonusStats()
        {
            Stats bonusStats = build.GetBuildStats();
            Window.BonusHealth = bonusStats.Health.ToString("0");
            Window.BonusHealthRegen = bonusStats.HealthRegen.ToString("0.0") + "/5s";
            Window.BonusMana = bonusStats.Mana.ToString("0");
            Window.BonusManaRegen = bonusStats.ManaRegen.ToString("0.0") + "/5s";
            Window.BonusArmor = bonusStats.Armor.ToString("0");
            Window.BonusMagicResist = bonusStats.MagicResist.ToString("0");
            Window.BonusMovementSpeed = (bonusStats.MovementSpeed * 100).ToString("0.0") + "%";
            Window.BonusAttackDamage = bonusStats.AttackDamage.ToString("0");
            Window.BonusAttackSpeed = (bonusStats.AttackSpeed * 100).ToString("0.0") + "%";
            Window.BonusCriticalChance = (bonusStats.CriticalChance * 100).ToString("0.0") + "%";
            Window.BonusLifesteal = (bonusStats.Lifesteal * 100).ToString("0.0") + "%";
            Window.BonusAbilityPower = bonusStats.AbilityPower.ToString("0");
            Window.BonusCooldownReduction = (bonusStats.CooldownReduction * 100).ToString("0") + "%";
            Window.BonusSpellvamp = (bonusStats.Spellvamp * 100).ToString("0.0") + "%";
        }
    }
}
