﻿using League_of_Stats.Views.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace LeagueOfStats.Views
{
    public partial class MainWindow : Form, IMainWindow
    {
        public event EventHandler<NameArgs> selectedChampionChanged;
        public event EventHandler searchChanged;
        public event EventHandler<NameArgs> itemInListClicked;
        public event EventHandler resetButtonClicked;
        public event EventHandler<NameArgs> item1Clicked;
        public event EventHandler<NameArgs> item2Clicked;
        public event EventHandler<NameArgs> item3Clicked;
        public event EventHandler<NameArgs> item4Clicked;
        public event EventHandler<NameArgs> item5Clicked;
        public event EventHandler<NameArgs> item6Clicked;

        public IList<string> ChampionsName
        {
            set
            {
                nameComboBox.Items.Clear();
                nameComboBox.Items.AddRange(value.ToArray());
                var source = new AutoCompleteStringCollection();
                source.Clear();
                source.AddRange(value.ToArray());
                nameComboBox.AutoCompleteCustomSource = source;
                nameComboBox.SelectedItem = nameComboBox.Items[0];
            }
        }

        public IList<string> ItemsName
        {
            set
            {
                itemListView.Items.Clear();
                foreach (string item in value)
                    itemListView.Items.Add(new ListViewItem() { Text = item });
            }
        }

        public string SearchText                { get { return searchField.Text; } }

        public string ChampionImageURL          { set { image.Load(value); } }
        public string Title                     { set { title.Text = value; } }
        public string Lore                      { set { loreBox.Text = value; } }

        public string Health                    { set { health.Text = value; } }
        public string HealthRegen               { set { healthRegen.Text = value; } }
        public string Mana                      { set { mana.Text = value; } }
        public string ManaRegen                 { set { manaRegen.Text = value; } }
        public string Armor                     { set { armor.Text = value; } }
        public string MagicResist               { set { magicResist.Text = value; } }
        public string MovementSpeed             { set { movementSpeed.Text = value; } }
        public string AttackDamage              { set { attackDamage.Text = value; } }
        public string AttackSpeed               { set { attackSpeed.Text = value; } }
        public string CriticalChance            { set { criticalChance.Text = value; } }
        public string Lifesteal                 { set { lifesteal.Text = value; } }
        public string AbilityPower              { set { abilityPower.Text = value; } }
        public string CooldownReduction         { set { cooldownReduction.Text = value; } }
        public string Spellvamp                 { set { spellvamp.Text = value; } }

        public string BonusHealth               { set { bonusHealth.Text = value; } }
        public string BonusHealthRegen          { set { bonusHealthRegen.Text = value; } }
        public string BonusMana                 { set { bonusMana.Text = value; } }
        public string BonusManaRegen            { set { bonusManaRegen.Text = value; } }
        public string BonusArmor                { set { bonusArmor.Text = value; } }
        public string BonusMagicResist          { set { bonusMagicResist.Text = value; } }
        public string BonusMovementSpeed        { set { bonusMovementSpeed.Text = value; } }
        public string BonusAttackDamage         { set { bonusAttackDamage.Text = value; } }
        public string BonusAttackSpeed          { set { bonusAttackSpeed.Text = value; } }
        public string BonusCriticalChance       { set { bonusCriticalChance.Text = value; } }
        public string BonusLifesteal            { set { bonusLifesteal.Text = value; } }
        public string BonusAbilityPower         { set { bonusAbilityPower.Text = value; } }
        public string BonusCooldownReduction    { set { bonusCooldownReduction.Text = value; } }
        public string BonusSpellvamp            { set { bonusSpellvamp.Text = value; } }

        public string Price                     { set { price.Text = value; } }

        public string Item1Image                { set { if (value != null) item1PictureBox.Load(value); else item1PictureBox.Image = null; } }
        public string Item2Image                { set { if (value != null) item2PictureBox.Load(value); else item2PictureBox.Image = null; } }
        public string Item3Image                { set { if (value != null) item3PictureBox.Load(value); else item3PictureBox.Image = null; } }
        public string Item4Image                { set { if (value != null) item4PictureBox.Load(value); else item4PictureBox.Image = null; } }
        public string Item5Image                { set { if (value != null) item5PictureBox.Load(value); else item5PictureBox.Image = null; } }
        public string Item6Image                { set { if (value != null) item6PictureBox.Load(value); else item6PictureBox.Image = null; } }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnChampionSelectedChanged(object sender, EventArgs e)
        {
            if (selectedChampionChanged != null)
                selectedChampionChanged(sender, new NameArgs() { Name = (string)nameComboBox.SelectedItem });
        }

        private void OnSearchFieldTextChanged(object sender, EventArgs e)
        {
            if (searchChanged != null)
                searchChanged(sender, e);
        }

        private void OnItemListViewItemActivate(object sender, EventArgs e)
        {
            if (itemInListClicked != null)
                itemInListClicked(sender, new NameArgs() { Name = itemListView.SelectedItems[0].Text });
        }

        private void OnResetButtonClick(object sender, EventArgs e)
        {
            if (resetButtonClicked != null)
                resetButtonClicked(sender, e);
        }

        private void OnItem1PictureBoxClick(object sender, EventArgs e)
        {
            if (item1Clicked != null)
                item1Clicked(sender, new NameArgs() { Name = "0" });
        }

        private void OnItem2PictureBoxClick(object sender, EventArgs e)
        {
            if (item2Clicked != null)
                item2Clicked(sender, new NameArgs() { Name = "1" });
        }

        private void OnItem3PictureBoxClick(object sender, EventArgs e)
        {
            if (item3Clicked != null)
                item3Clicked(sender, new NameArgs() { Name = "2" });
        }

        private void OnItem4PictureBoxClick(object sender, EventArgs e)
        {
            if (item4Clicked != null)
                item4Clicked(sender, new NameArgs() { Name = "3" });
        }

        private void OnItem5PictureBoxClick(object sender, EventArgs e)
        {
            if (item5Clicked != null)
                item5Clicked(sender, new NameArgs() { Name = "4" });
        }

        private void OnItem6PictureBoxClick(object sender, EventArgs e)
        {
            if (item6Clicked != null)
                item6Clicked(sender, new NameArgs() { Name = "5" });
        }

        public void ShowError(string text)
        {
            if(MessageBox.Show(text, "Error", MessageBoxButtons.OK) == DialogResult.OK)
            {
                Close();
            }
        }
    }
}
