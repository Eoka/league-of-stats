﻿namespace LeagueOfStats.Views
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.championTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.championHeaderTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.namePictureTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.image = new System.Windows.Forms.PictureBox();
            this.nameComboBox = new System.Windows.Forms.ComboBox();
            this.titleLoreTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.title = new System.Windows.Forms.Label();
            this.loreBox = new System.Windows.Forms.RichTextBox();
            this.statsTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.rightStatsTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.defenseUtilityTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.defenseUtilityStatsTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.bonusMovementSpeed = new System.Windows.Forms.Label();
            this.bonusMagicResist = new System.Windows.Forms.Label();
            this.armorLabel = new System.Windows.Forms.Label();
            this.magicResistLabel = new System.Windows.Forms.Label();
            this.movementSpeedLabel = new System.Windows.Forms.Label();
            this.armor = new System.Windows.Forms.Label();
            this.magicResist = new System.Windows.Forms.Label();
            this.movementSpeed = new System.Windows.Forms.Label();
            this.bonusArmor = new System.Windows.Forms.Label();
            this.defenseUtilityTitle = new System.Windows.Forms.Label();
            this.abilityPowerTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.abilityPowerStatsTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.bonusSpellvamp = new System.Windows.Forms.Label();
            this.bonusCooldownReduction = new System.Windows.Forms.Label();
            this.abilityPowerLabel = new System.Windows.Forms.Label();
            this.coolddownReductionLabel = new System.Windows.Forms.Label();
            this.spellVampLabel = new System.Windows.Forms.Label();
            this.abilityPower = new System.Windows.Forms.Label();
            this.cooldownReduction = new System.Windows.Forms.Label();
            this.spellvamp = new System.Windows.Forms.Label();
            this.bonusAbilityPower = new System.Windows.Forms.Label();
            this.abilityPowerTitle = new System.Windows.Forms.Label();
            this.leftStatsTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.healthManaTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.healthManaTitle = new System.Windows.Forms.Label();
            this.healthManaStatsTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.bonusManaRegen = new System.Windows.Forms.Label();
            this.bonusMana = new System.Windows.Forms.Label();
            this.bonusHealthRegen = new System.Windows.Forms.Label();
            this.healthLabel = new System.Windows.Forms.Label();
            this.healthRegenLabel = new System.Windows.Forms.Label();
            this.manaLabel = new System.Windows.Forms.Label();
            this.manaRegenLabel = new System.Windows.Forms.Label();
            this.health = new System.Windows.Forms.Label();
            this.healthRegen = new System.Windows.Forms.Label();
            this.mana = new System.Windows.Forms.Label();
            this.manaRegen = new System.Windows.Forms.Label();
            this.bonusHealth = new System.Windows.Forms.Label();
            this.attackDamageTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.attackDamageTitle = new System.Windows.Forms.Label();
            this.attackDamageStatsTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.bonusLifesteal = new System.Windows.Forms.Label();
            this.bonusCriticalChance = new System.Windows.Forms.Label();
            this.bonusAttackSpeed = new System.Windows.Forms.Label();
            this.attackDamageLabel = new System.Windows.Forms.Label();
            this.attackSpeedLabel = new System.Windows.Forms.Label();
            this.criticalChanceLabel = new System.Windows.Forms.Label();
            this.lifestealLabel = new System.Windows.Forms.Label();
            this.attackDamage = new System.Windows.Forms.Label();
            this.attackSpeed = new System.Windows.Forms.Label();
            this.criticalChance = new System.Windows.Forms.Label();
            this.lifesteal = new System.Windows.Forms.Label();
            this.bonusAttackDamage = new System.Windows.Forms.Label();
            this.itemTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.buildTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.buildCaseTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.item4PictureBox = new System.Windows.Forms.PictureBox();
            this.item5PictureBox = new System.Windows.Forms.PictureBox();
            this.item6PictureBox = new System.Windows.Forms.PictureBox();
            this.item1PictureBox = new System.Windows.Forms.PictureBox();
            this.item2PictureBox = new System.Windows.Forms.PictureBox();
            this.item3PictureBox = new System.Windows.Forms.PictureBox();
            this.buildActionsTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.price = new System.Windows.Forms.Label();
            this.resetButton = new System.Windows.Forms.Button();
            this.researchTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.searchField = new System.Windows.Forms.TextBox();
            this.itemListView = new System.Windows.Forms.ListView();
            this.mainTableLayout.SuspendLayout();
            this.championTableLayout.SuspendLayout();
            this.championHeaderTableLayout.SuspendLayout();
            this.namePictureTableLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.image)).BeginInit();
            this.titleLoreTableLayout.SuspendLayout();
            this.statsTableLayout.SuspendLayout();
            this.rightStatsTableLayout.SuspendLayout();
            this.defenseUtilityTableLayout.SuspendLayout();
            this.defenseUtilityStatsTableLayout.SuspendLayout();
            this.abilityPowerTableLayout.SuspendLayout();
            this.abilityPowerStatsTableLayout.SuspendLayout();
            this.leftStatsTableLayout.SuspendLayout();
            this.healthManaTableLayout.SuspendLayout();
            this.healthManaStatsTableLayout.SuspendLayout();
            this.attackDamageTableLayout.SuspendLayout();
            this.attackDamageStatsTableLayout.SuspendLayout();
            this.itemTableLayout.SuspendLayout();
            this.buildTableLayout.SuspendLayout();
            this.buildCaseTableLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.item4PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item5PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item6PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item2PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item3PictureBox)).BeginInit();
            this.buildActionsTableLayout.SuspendLayout();
            this.researchTableLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTableLayout
            // 
            this.mainTableLayout.ColumnCount = 2;
            this.mainTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 500F));
            this.mainTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTableLayout.Controls.Add(this.championTableLayout, 0, 0);
            this.mainTableLayout.Controls.Add(this.itemTableLayout, 1, 0);
            this.mainTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTableLayout.Location = new System.Drawing.Point(0, 0);
            this.mainTableLayout.Name = "mainTableLayout";
            this.mainTableLayout.RowCount = 1;
            this.mainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTableLayout.Size = new System.Drawing.Size(825, 386);
            this.mainTableLayout.TabIndex = 0;
            // 
            // championTableLayout
            // 
            this.championTableLayout.ColumnCount = 1;
            this.championTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.championTableLayout.Controls.Add(this.championHeaderTableLayout, 0, 0);
            this.championTableLayout.Controls.Add(this.statsTableLayout, 0, 1);
            this.championTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.championTableLayout.Location = new System.Drawing.Point(0, 0);
            this.championTableLayout.Margin = new System.Windows.Forms.Padding(0);
            this.championTableLayout.Name = "championTableLayout";
            this.championTableLayout.RowCount = 2;
            this.championTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.championTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.championTableLayout.Size = new System.Drawing.Size(500, 386);
            this.championTableLayout.TabIndex = 0;
            // 
            // championHeaderTableLayout
            // 
            this.championHeaderTableLayout.ColumnCount = 2;
            this.championHeaderTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 135F));
            this.championHeaderTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.championHeaderTableLayout.Controls.Add(this.namePictureTableLayout, 0, 0);
            this.championHeaderTableLayout.Controls.Add(this.titleLoreTableLayout, 1, 0);
            this.championHeaderTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.championHeaderTableLayout.Location = new System.Drawing.Point(0, 0);
            this.championHeaderTableLayout.Margin = new System.Windows.Forms.Padding(0);
            this.championHeaderTableLayout.Name = "championHeaderTableLayout";
            this.championHeaderTableLayout.RowCount = 1;
            this.championHeaderTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.championHeaderTableLayout.Size = new System.Drawing.Size(500, 150);
            this.championHeaderTableLayout.TabIndex = 0;
            // 
            // namePictureTableLayout
            // 
            this.namePictureTableLayout.ColumnCount = 1;
            this.namePictureTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.namePictureTableLayout.Controls.Add(this.image, 0, 1);
            this.namePictureTableLayout.Controls.Add(this.nameComboBox, 0, 0);
            this.namePictureTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.namePictureTableLayout.Location = new System.Drawing.Point(0, 0);
            this.namePictureTableLayout.Margin = new System.Windows.Forms.Padding(0);
            this.namePictureTableLayout.Name = "namePictureTableLayout";
            this.namePictureTableLayout.RowCount = 2;
            this.namePictureTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.namePictureTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.namePictureTableLayout.Size = new System.Drawing.Size(135, 150);
            this.namePictureTableLayout.TabIndex = 0;
            // 
            // image
            // 
            this.image.Dock = System.Windows.Forms.DockStyle.Fill;
            this.image.Location = new System.Drawing.Point(0, 25);
            this.image.Margin = new System.Windows.Forms.Padding(0);
            this.image.Name = "image";
            this.image.Size = new System.Drawing.Size(135, 125);
            this.image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.image.TabIndex = 0;
            this.image.TabStop = false;
            // 
            // nameComboBox
            // 
            this.nameComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.nameComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.nameComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nameComboBox.FormattingEnabled = true;
            this.nameComboBox.Location = new System.Drawing.Point(3, 3);
            this.nameComboBox.Name = "nameComboBox";
            this.nameComboBox.Size = new System.Drawing.Size(129, 21);
            this.nameComboBox.Sorted = true;
            this.nameComboBox.TabIndex = 1;
            this.nameComboBox.SelectedIndexChanged += new System.EventHandler(this.OnChampionSelectedChanged);
            // 
            // titleLoreTableLayout
            // 
            this.titleLoreTableLayout.ColumnCount = 1;
            this.titleLoreTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.titleLoreTableLayout.Controls.Add(this.title, 0, 0);
            this.titleLoreTableLayout.Controls.Add(this.loreBox, 0, 1);
            this.titleLoreTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.titleLoreTableLayout.Location = new System.Drawing.Point(135, 0);
            this.titleLoreTableLayout.Margin = new System.Windows.Forms.Padding(0);
            this.titleLoreTableLayout.Name = "titleLoreTableLayout";
            this.titleLoreTableLayout.RowCount = 2;
            this.titleLoreTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.titleLoreTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.titleLoreTableLayout.Size = new System.Drawing.Size(365, 150);
            this.titleLoreTableLayout.TabIndex = 1;
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.Dock = System.Windows.Forms.DockStyle.Fill;
            this.title.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.Location = new System.Drawing.Point(3, 0);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(359, 35);
            this.title.TabIndex = 0;
            this.title.Text = "Title";
            this.title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // loreBox
            // 
            this.loreBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.loreBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loreBox.Location = new System.Drawing.Point(3, 38);
            this.loreBox.Name = "loreBox";
            this.loreBox.ReadOnly = true;
            this.loreBox.Size = new System.Drawing.Size(359, 109);
            this.loreBox.TabIndex = 1;
            this.loreBox.Text = "";
            // 
            // statsTableLayout
            // 
            this.statsTableLayout.ColumnCount = 2;
            this.statsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.statsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.statsTableLayout.Controls.Add(this.rightStatsTableLayout, 0, 0);
            this.statsTableLayout.Controls.Add(this.leftStatsTableLayout, 0, 0);
            this.statsTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statsTableLayout.Location = new System.Drawing.Point(0, 150);
            this.statsTableLayout.Margin = new System.Windows.Forms.Padding(0);
            this.statsTableLayout.Name = "statsTableLayout";
            this.statsTableLayout.RowCount = 1;
            this.statsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.statsTableLayout.Size = new System.Drawing.Size(500, 236);
            this.statsTableLayout.TabIndex = 1;
            // 
            // rightStatsTableLayout
            // 
            this.rightStatsTableLayout.ColumnCount = 1;
            this.rightStatsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.rightStatsTableLayout.Controls.Add(this.defenseUtilityTableLayout, 0, 0);
            this.rightStatsTableLayout.Controls.Add(this.abilityPowerTableLayout, 0, 1);
            this.rightStatsTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightStatsTableLayout.Location = new System.Drawing.Point(250, 0);
            this.rightStatsTableLayout.Margin = new System.Windows.Forms.Padding(0);
            this.rightStatsTableLayout.Name = "rightStatsTableLayout";
            this.rightStatsTableLayout.RowCount = 3;
            this.rightStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.rightStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.rightStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.rightStatsTableLayout.Size = new System.Drawing.Size(250, 236);
            this.rightStatsTableLayout.TabIndex = 1;
            // 
            // defenseUtilityTableLayout
            // 
            this.defenseUtilityTableLayout.ColumnCount = 1;
            this.defenseUtilityTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.defenseUtilityTableLayout.Controls.Add(this.defenseUtilityStatsTableLayout, 0, 1);
            this.defenseUtilityTableLayout.Controls.Add(this.defenseUtilityTitle, 0, 0);
            this.defenseUtilityTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.defenseUtilityTableLayout.Location = new System.Drawing.Point(3, 3);
            this.defenseUtilityTableLayout.Name = "defenseUtilityTableLayout";
            this.defenseUtilityTableLayout.RowCount = 2;
            this.defenseUtilityTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.defenseUtilityTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.defenseUtilityTableLayout.Size = new System.Drawing.Size(244, 109);
            this.defenseUtilityTableLayout.TabIndex = 1;
            // 
            // defenseUtilityStatsTableLayout
            // 
            this.defenseUtilityStatsTableLayout.ColumnCount = 3;
            this.defenseUtilityStatsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.defenseUtilityStatsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.defenseUtilityStatsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.defenseUtilityStatsTableLayout.Controls.Add(this.bonusMovementSpeed, 2, 2);
            this.defenseUtilityStatsTableLayout.Controls.Add(this.bonusMagicResist, 2, 1);
            this.defenseUtilityStatsTableLayout.Controls.Add(this.armorLabel, 0, 0);
            this.defenseUtilityStatsTableLayout.Controls.Add(this.magicResistLabel, 0, 1);
            this.defenseUtilityStatsTableLayout.Controls.Add(this.movementSpeedLabel, 0, 2);
            this.defenseUtilityStatsTableLayout.Controls.Add(this.armor, 1, 0);
            this.defenseUtilityStatsTableLayout.Controls.Add(this.magicResist, 1, 1);
            this.defenseUtilityStatsTableLayout.Controls.Add(this.movementSpeed, 1, 2);
            this.defenseUtilityStatsTableLayout.Controls.Add(this.bonusArmor, 2, 0);
            this.defenseUtilityStatsTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.defenseUtilityStatsTableLayout.Location = new System.Drawing.Point(3, 28);
            this.defenseUtilityStatsTableLayout.Name = "defenseUtilityStatsTableLayout";
            this.defenseUtilityStatsTableLayout.RowCount = 4;
            this.defenseUtilityStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.defenseUtilityStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.defenseUtilityStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.defenseUtilityStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.defenseUtilityStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.defenseUtilityStatsTableLayout.Size = new System.Drawing.Size(238, 78);
            this.defenseUtilityStatsTableLayout.TabIndex = 2;
            // 
            // bonusMovementSpeed
            // 
            this.bonusMovementSpeed.AutoSize = true;
            this.bonusMovementSpeed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bonusMovementSpeed.ForeColor = System.Drawing.Color.SaddleBrown;
            this.bonusMovementSpeed.Location = new System.Drawing.Point(177, 40);
            this.bonusMovementSpeed.Name = "bonusMovementSpeed";
            this.bonusMovementSpeed.Size = new System.Drawing.Size(58, 20);
            this.bonusMovementSpeed.TabIndex = 9;
            this.bonusMovementSpeed.Text = "bms";
            this.bonusMovementSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bonusMagicResist
            // 
            this.bonusMagicResist.AutoSize = true;
            this.bonusMagicResist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bonusMagicResist.ForeColor = System.Drawing.Color.SlateGray;
            this.bonusMagicResist.Location = new System.Drawing.Point(177, 20);
            this.bonusMagicResist.Name = "bonusMagicResist";
            this.bonusMagicResist.Size = new System.Drawing.Size(58, 20);
            this.bonusMagicResist.TabIndex = 8;
            this.bonusMagicResist.Text = "bmr";
            this.bonusMagicResist.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // armorLabel
            // 
            this.armorLabel.AutoSize = true;
            this.armorLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.armorLabel.Location = new System.Drawing.Point(3, 0);
            this.armorLabel.Name = "armorLabel";
            this.armorLabel.Size = new System.Drawing.Size(104, 20);
            this.armorLabel.TabIndex = 0;
            this.armorLabel.Text = "Armor :";
            this.armorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // magicResistLabel
            // 
            this.magicResistLabel.AutoSize = true;
            this.magicResistLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.magicResistLabel.Location = new System.Drawing.Point(3, 20);
            this.magicResistLabel.Name = "magicResistLabel";
            this.magicResistLabel.Size = new System.Drawing.Size(104, 20);
            this.magicResistLabel.TabIndex = 1;
            this.magicResistLabel.Text = "Magic Resist :";
            this.magicResistLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // movementSpeedLabel
            // 
            this.movementSpeedLabel.AutoSize = true;
            this.movementSpeedLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.movementSpeedLabel.Location = new System.Drawing.Point(3, 40);
            this.movementSpeedLabel.Name = "movementSpeedLabel";
            this.movementSpeedLabel.Size = new System.Drawing.Size(104, 20);
            this.movementSpeedLabel.TabIndex = 2;
            this.movementSpeedLabel.Text = "Movement Speed :";
            this.movementSpeedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // armor
            // 
            this.armor.AutoSize = true;
            this.armor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.armor.Location = new System.Drawing.Point(113, 0);
            this.armor.Name = "armor";
            this.armor.Size = new System.Drawing.Size(58, 20);
            this.armor.TabIndex = 4;
            this.armor.Text = "arm";
            this.armor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // magicResist
            // 
            this.magicResist.AutoSize = true;
            this.magicResist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.magicResist.Location = new System.Drawing.Point(113, 20);
            this.magicResist.Name = "magicResist";
            this.magicResist.Size = new System.Drawing.Size(58, 20);
            this.magicResist.TabIndex = 5;
            this.magicResist.Text = "mr";
            this.magicResist.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // movementSpeed
            // 
            this.movementSpeed.AutoSize = true;
            this.movementSpeed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.movementSpeed.Location = new System.Drawing.Point(113, 40);
            this.movementSpeed.Name = "movementSpeed";
            this.movementSpeed.Size = new System.Drawing.Size(58, 20);
            this.movementSpeed.TabIndex = 6;
            this.movementSpeed.Text = "ms";
            this.movementSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bonusArmor
            // 
            this.bonusArmor.AutoSize = true;
            this.bonusArmor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bonusArmor.ForeColor = System.Drawing.Color.DimGray;
            this.bonusArmor.Location = new System.Drawing.Point(177, 0);
            this.bonusArmor.Name = "bonusArmor";
            this.bonusArmor.Size = new System.Drawing.Size(58, 20);
            this.bonusArmor.TabIndex = 7;
            this.bonusArmor.Text = "barm";
            this.bonusArmor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // defenseUtilityTitle
            // 
            this.defenseUtilityTitle.AutoSize = true;
            this.defenseUtilityTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.defenseUtilityTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defenseUtilityTitle.Location = new System.Drawing.Point(3, 0);
            this.defenseUtilityTitle.Name = "defenseUtilityTitle";
            this.defenseUtilityTitle.Size = new System.Drawing.Size(238, 25);
            this.defenseUtilityTitle.TabIndex = 1;
            this.defenseUtilityTitle.Text = "Defense & Utility :";
            this.defenseUtilityTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.defenseUtilityTitle.UseMnemonic = false;
            // 
            // abilityPowerTableLayout
            // 
            this.abilityPowerTableLayout.ColumnCount = 1;
            this.abilityPowerTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.abilityPowerTableLayout.Controls.Add(this.abilityPowerStatsTableLayout, 0, 1);
            this.abilityPowerTableLayout.Controls.Add(this.abilityPowerTitle, 0, 0);
            this.abilityPowerTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.abilityPowerTableLayout.Location = new System.Drawing.Point(3, 118);
            this.abilityPowerTableLayout.Name = "abilityPowerTableLayout";
            this.abilityPowerTableLayout.RowCount = 2;
            this.abilityPowerTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.abilityPowerTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.abilityPowerTableLayout.Size = new System.Drawing.Size(244, 109);
            this.abilityPowerTableLayout.TabIndex = 1;
            // 
            // abilityPowerStatsTableLayout
            // 
            this.abilityPowerStatsTableLayout.ColumnCount = 3;
            this.abilityPowerStatsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.abilityPowerStatsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.abilityPowerStatsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.abilityPowerStatsTableLayout.Controls.Add(this.bonusSpellvamp, 2, 2);
            this.abilityPowerStatsTableLayout.Controls.Add(this.bonusCooldownReduction, 2, 1);
            this.abilityPowerStatsTableLayout.Controls.Add(this.abilityPowerLabel, 0, 0);
            this.abilityPowerStatsTableLayout.Controls.Add(this.coolddownReductionLabel, 0, 1);
            this.abilityPowerStatsTableLayout.Controls.Add(this.spellVampLabel, 0, 2);
            this.abilityPowerStatsTableLayout.Controls.Add(this.abilityPower, 1, 0);
            this.abilityPowerStatsTableLayout.Controls.Add(this.cooldownReduction, 1, 1);
            this.abilityPowerStatsTableLayout.Controls.Add(this.spellvamp, 1, 2);
            this.abilityPowerStatsTableLayout.Controls.Add(this.bonusAbilityPower, 2, 0);
            this.abilityPowerStatsTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.abilityPowerStatsTableLayout.Location = new System.Drawing.Point(3, 28);
            this.abilityPowerStatsTableLayout.Name = "abilityPowerStatsTableLayout";
            this.abilityPowerStatsTableLayout.RowCount = 4;
            this.abilityPowerStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.abilityPowerStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.abilityPowerStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.abilityPowerStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.abilityPowerStatsTableLayout.Size = new System.Drawing.Size(238, 78);
            this.abilityPowerStatsTableLayout.TabIndex = 2;
            // 
            // bonusSpellvamp
            // 
            this.bonusSpellvamp.AutoSize = true;
            this.bonusSpellvamp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bonusSpellvamp.ForeColor = System.Drawing.Color.Indigo;
            this.bonusSpellvamp.Location = new System.Drawing.Point(182, 40);
            this.bonusSpellvamp.Name = "bonusSpellvamp";
            this.bonusSpellvamp.Size = new System.Drawing.Size(53, 20);
            this.bonusSpellvamp.TabIndex = 9;
            this.bonusSpellvamp.Text = "bsv";
            this.bonusSpellvamp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bonusCooldownReduction
            // 
            this.bonusCooldownReduction.AutoSize = true;
            this.bonusCooldownReduction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bonusCooldownReduction.ForeColor = System.Drawing.Color.MediumOrchid;
            this.bonusCooldownReduction.Location = new System.Drawing.Point(182, 20);
            this.bonusCooldownReduction.Name = "bonusCooldownReduction";
            this.bonusCooldownReduction.Size = new System.Drawing.Size(53, 20);
            this.bonusCooldownReduction.TabIndex = 8;
            this.bonusCooldownReduction.Text = "bcdr";
            this.bonusCooldownReduction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // abilityPowerLabel
            // 
            this.abilityPowerLabel.AutoSize = true;
            this.abilityPowerLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.abilityPowerLabel.Location = new System.Drawing.Point(3, 0);
            this.abilityPowerLabel.Name = "abilityPowerLabel";
            this.abilityPowerLabel.Size = new System.Drawing.Size(114, 20);
            this.abilityPowerLabel.TabIndex = 0;
            this.abilityPowerLabel.Text = "Ability power :";
            this.abilityPowerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // coolddownReductionLabel
            // 
            this.coolddownReductionLabel.AutoSize = true;
            this.coolddownReductionLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.coolddownReductionLabel.Location = new System.Drawing.Point(3, 20);
            this.coolddownReductionLabel.Name = "coolddownReductionLabel";
            this.coolddownReductionLabel.Size = new System.Drawing.Size(114, 20);
            this.coolddownReductionLabel.TabIndex = 1;
            this.coolddownReductionLabel.Text = "Cooldown reduction :";
            this.coolddownReductionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // spellVampLabel
            // 
            this.spellVampLabel.AutoSize = true;
            this.spellVampLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spellVampLabel.Location = new System.Drawing.Point(3, 40);
            this.spellVampLabel.Name = "spellVampLabel";
            this.spellVampLabel.Size = new System.Drawing.Size(114, 20);
            this.spellVampLabel.TabIndex = 2;
            this.spellVampLabel.Text = "Spell vampirism:";
            this.spellVampLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // abilityPower
            // 
            this.abilityPower.AutoSize = true;
            this.abilityPower.Dock = System.Windows.Forms.DockStyle.Fill;
            this.abilityPower.Location = new System.Drawing.Point(123, 0);
            this.abilityPower.Name = "abilityPower";
            this.abilityPower.Size = new System.Drawing.Size(53, 20);
            this.abilityPower.TabIndex = 4;
            this.abilityPower.Text = "ap";
            this.abilityPower.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cooldownReduction
            // 
            this.cooldownReduction.AutoSize = true;
            this.cooldownReduction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cooldownReduction.Location = new System.Drawing.Point(123, 20);
            this.cooldownReduction.Name = "cooldownReduction";
            this.cooldownReduction.Size = new System.Drawing.Size(53, 20);
            this.cooldownReduction.TabIndex = 5;
            this.cooldownReduction.Text = "cdr";
            this.cooldownReduction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // spellvamp
            // 
            this.spellvamp.AutoSize = true;
            this.spellvamp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spellvamp.Location = new System.Drawing.Point(123, 40);
            this.spellvamp.Name = "spellvamp";
            this.spellvamp.Size = new System.Drawing.Size(53, 20);
            this.spellvamp.TabIndex = 6;
            this.spellvamp.Text = "sv";
            this.spellvamp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bonusAbilityPower
            // 
            this.bonusAbilityPower.AutoSize = true;
            this.bonusAbilityPower.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bonusAbilityPower.ForeColor = System.Drawing.Color.MediumBlue;
            this.bonusAbilityPower.Location = new System.Drawing.Point(182, 0);
            this.bonusAbilityPower.Name = "bonusAbilityPower";
            this.bonusAbilityPower.Size = new System.Drawing.Size(53, 20);
            this.bonusAbilityPower.TabIndex = 7;
            this.bonusAbilityPower.Text = "bap";
            this.bonusAbilityPower.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // abilityPowerTitle
            // 
            this.abilityPowerTitle.AutoSize = true;
            this.abilityPowerTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.abilityPowerTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.abilityPowerTitle.Location = new System.Drawing.Point(3, 0);
            this.abilityPowerTitle.Name = "abilityPowerTitle";
            this.abilityPowerTitle.Size = new System.Drawing.Size(238, 25);
            this.abilityPowerTitle.TabIndex = 1;
            this.abilityPowerTitle.Text = "Ability Power :";
            this.abilityPowerTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.abilityPowerTitle.UseMnemonic = false;
            // 
            // leftStatsTableLayout
            // 
            this.leftStatsTableLayout.ColumnCount = 1;
            this.leftStatsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.leftStatsTableLayout.Controls.Add(this.healthManaTableLayout, 0, 0);
            this.leftStatsTableLayout.Controls.Add(this.attackDamageTableLayout, 0, 1);
            this.leftStatsTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftStatsTableLayout.Location = new System.Drawing.Point(0, 0);
            this.leftStatsTableLayout.Margin = new System.Windows.Forms.Padding(0);
            this.leftStatsTableLayout.Name = "leftStatsTableLayout";
            this.leftStatsTableLayout.RowCount = 3;
            this.leftStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.leftStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.leftStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.leftStatsTableLayout.Size = new System.Drawing.Size(250, 236);
            this.leftStatsTableLayout.TabIndex = 0;
            // 
            // healthManaTableLayout
            // 
            this.healthManaTableLayout.ColumnCount = 1;
            this.healthManaTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.healthManaTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.healthManaTableLayout.Controls.Add(this.healthManaTitle, 0, 0);
            this.healthManaTableLayout.Controls.Add(this.healthManaStatsTableLayout, 0, 1);
            this.healthManaTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.healthManaTableLayout.Location = new System.Drawing.Point(3, 3);
            this.healthManaTableLayout.Name = "healthManaTableLayout";
            this.healthManaTableLayout.RowCount = 2;
            this.healthManaTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.healthManaTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.healthManaTableLayout.Size = new System.Drawing.Size(244, 109);
            this.healthManaTableLayout.TabIndex = 0;
            // 
            // healthManaTitle
            // 
            this.healthManaTitle.AutoSize = true;
            this.healthManaTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.healthManaTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.healthManaTitle.Location = new System.Drawing.Point(3, 0);
            this.healthManaTitle.Name = "healthManaTitle";
            this.healthManaTitle.Size = new System.Drawing.Size(238, 25);
            this.healthManaTitle.TabIndex = 0;
            this.healthManaTitle.Text = "Health & Mana :";
            this.healthManaTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.healthManaTitle.UseMnemonic = false;
            // 
            // healthManaStatsTableLayout
            // 
            this.healthManaStatsTableLayout.ColumnCount = 3;
            this.healthManaStatsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.healthManaStatsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.healthManaStatsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.healthManaStatsTableLayout.Controls.Add(this.bonusManaRegen, 2, 3);
            this.healthManaStatsTableLayout.Controls.Add(this.bonusMana, 2, 2);
            this.healthManaStatsTableLayout.Controls.Add(this.bonusHealthRegen, 2, 1);
            this.healthManaStatsTableLayout.Controls.Add(this.healthLabel, 0, 0);
            this.healthManaStatsTableLayout.Controls.Add(this.healthRegenLabel, 0, 1);
            this.healthManaStatsTableLayout.Controls.Add(this.manaLabel, 0, 2);
            this.healthManaStatsTableLayout.Controls.Add(this.manaRegenLabel, 0, 3);
            this.healthManaStatsTableLayout.Controls.Add(this.health, 1, 0);
            this.healthManaStatsTableLayout.Controls.Add(this.healthRegen, 1, 1);
            this.healthManaStatsTableLayout.Controls.Add(this.mana, 1, 2);
            this.healthManaStatsTableLayout.Controls.Add(this.manaRegen, 1, 3);
            this.healthManaStatsTableLayout.Controls.Add(this.bonusHealth, 2, 0);
            this.healthManaStatsTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.healthManaStatsTableLayout.Location = new System.Drawing.Point(3, 28);
            this.healthManaStatsTableLayout.Name = "healthManaStatsTableLayout";
            this.healthManaStatsTableLayout.RowCount = 5;
            this.healthManaStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.healthManaStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.healthManaStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.healthManaStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.healthManaStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.healthManaStatsTableLayout.Size = new System.Drawing.Size(238, 78);
            this.healthManaStatsTableLayout.TabIndex = 1;
            // 
            // bonusManaRegen
            // 
            this.bonusManaRegen.AutoSize = true;
            this.bonusManaRegen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bonusManaRegen.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.bonusManaRegen.Location = new System.Drawing.Point(182, 60);
            this.bonusManaRegen.Name = "bonusManaRegen";
            this.bonusManaRegen.Size = new System.Drawing.Size(53, 20);
            this.bonusManaRegen.TabIndex = 11;
            this.bonusManaRegen.Text = "bmrgn";
            this.bonusManaRegen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bonusMana
            // 
            this.bonusMana.AutoSize = true;
            this.bonusMana.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bonusMana.ForeColor = System.Drawing.Color.SteelBlue;
            this.bonusMana.Location = new System.Drawing.Point(182, 40);
            this.bonusMana.Name = "bonusMana";
            this.bonusMana.Size = new System.Drawing.Size(53, 20);
            this.bonusMana.TabIndex = 10;
            this.bonusMana.Text = "bm";
            this.bonusMana.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bonusHealthRegen
            // 
            this.bonusHealthRegen.AutoSize = true;
            this.bonusHealthRegen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bonusHealthRegen.ForeColor = System.Drawing.Color.ForestGreen;
            this.bonusHealthRegen.Location = new System.Drawing.Point(182, 20);
            this.bonusHealthRegen.Name = "bonusHealthRegen";
            this.bonusHealthRegen.Size = new System.Drawing.Size(53, 20);
            this.bonusHealthRegen.TabIndex = 9;
            this.bonusHealthRegen.Text = "bhrgn";
            this.bonusHealthRegen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // healthLabel
            // 
            this.healthLabel.AutoSize = true;
            this.healthLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.healthLabel.Location = new System.Drawing.Point(3, 0);
            this.healthLabel.Name = "healthLabel";
            this.healthLabel.Size = new System.Drawing.Size(114, 20);
            this.healthLabel.TabIndex = 0;
            this.healthLabel.Text = "Health :";
            this.healthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // healthRegenLabel
            // 
            this.healthRegenLabel.AutoSize = true;
            this.healthRegenLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.healthRegenLabel.Location = new System.Drawing.Point(3, 20);
            this.healthRegenLabel.Name = "healthRegenLabel";
            this.healthRegenLabel.Size = new System.Drawing.Size(114, 20);
            this.healthRegenLabel.TabIndex = 1;
            this.healthRegenLabel.Text = "Health regen :";
            this.healthRegenLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // manaLabel
            // 
            this.manaLabel.AutoSize = true;
            this.manaLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.manaLabel.Location = new System.Drawing.Point(3, 40);
            this.manaLabel.Name = "manaLabel";
            this.manaLabel.Size = new System.Drawing.Size(114, 20);
            this.manaLabel.TabIndex = 2;
            this.manaLabel.Text = "Mana :";
            this.manaLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // manaRegenLabel
            // 
            this.manaRegenLabel.AutoSize = true;
            this.manaRegenLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.manaRegenLabel.Location = new System.Drawing.Point(3, 60);
            this.manaRegenLabel.Name = "manaRegenLabel";
            this.manaRegenLabel.Size = new System.Drawing.Size(114, 20);
            this.manaRegenLabel.TabIndex = 3;
            this.manaRegenLabel.Text = "Mana regen :";
            this.manaRegenLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // health
            // 
            this.health.AutoSize = true;
            this.health.Dock = System.Windows.Forms.DockStyle.Fill;
            this.health.Location = new System.Drawing.Point(123, 0);
            this.health.Name = "health";
            this.health.Size = new System.Drawing.Size(53, 20);
            this.health.TabIndex = 4;
            this.health.Text = "h";
            this.health.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // healthRegen
            // 
            this.healthRegen.AutoSize = true;
            this.healthRegen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.healthRegen.Location = new System.Drawing.Point(123, 20);
            this.healthRegen.Name = "healthRegen";
            this.healthRegen.Size = new System.Drawing.Size(53, 20);
            this.healthRegen.TabIndex = 5;
            this.healthRegen.Text = "hrgn";
            this.healthRegen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mana
            // 
            this.mana.AutoSize = true;
            this.mana.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mana.Location = new System.Drawing.Point(123, 40);
            this.mana.Name = "mana";
            this.mana.Size = new System.Drawing.Size(53, 20);
            this.mana.TabIndex = 6;
            this.mana.Text = "m";
            this.mana.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // manaRegen
            // 
            this.manaRegen.AutoSize = true;
            this.manaRegen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.manaRegen.Location = new System.Drawing.Point(123, 60);
            this.manaRegen.Name = "manaRegen";
            this.manaRegen.Size = new System.Drawing.Size(53, 20);
            this.manaRegen.TabIndex = 7;
            this.manaRegen.Text = "mrgn";
            this.manaRegen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bonusHealth
            // 
            this.bonusHealth.AutoSize = true;
            this.bonusHealth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bonusHealth.ForeColor = System.Drawing.Color.DarkGreen;
            this.bonusHealth.Location = new System.Drawing.Point(182, 0);
            this.bonusHealth.Name = "bonusHealth";
            this.bonusHealth.Size = new System.Drawing.Size(53, 20);
            this.bonusHealth.TabIndex = 8;
            this.bonusHealth.Text = "bh";
            this.bonusHealth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // attackDamageTableLayout
            // 
            this.attackDamageTableLayout.ColumnCount = 1;
            this.attackDamageTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.attackDamageTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.attackDamageTableLayout.Controls.Add(this.attackDamageTitle, 0, 0);
            this.attackDamageTableLayout.Controls.Add(this.attackDamageStatsTableLayout, 0, 1);
            this.attackDamageTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attackDamageTableLayout.Location = new System.Drawing.Point(3, 118);
            this.attackDamageTableLayout.Name = "attackDamageTableLayout";
            this.attackDamageTableLayout.RowCount = 2;
            this.attackDamageTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.attackDamageTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.attackDamageTableLayout.Size = new System.Drawing.Size(244, 109);
            this.attackDamageTableLayout.TabIndex = 0;
            // 
            // attackDamageTitle
            // 
            this.attackDamageTitle.AutoSize = true;
            this.attackDamageTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attackDamageTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attackDamageTitle.Location = new System.Drawing.Point(3, 0);
            this.attackDamageTitle.Name = "attackDamageTitle";
            this.attackDamageTitle.Size = new System.Drawing.Size(238, 25);
            this.attackDamageTitle.TabIndex = 0;
            this.attackDamageTitle.Text = "Attack Damage : ";
            this.attackDamageTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.attackDamageTitle.UseMnemonic = false;
            // 
            // attackDamageStatsTableLayout
            // 
            this.attackDamageStatsTableLayout.ColumnCount = 3;
            this.attackDamageStatsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.attackDamageStatsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.attackDamageStatsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.attackDamageStatsTableLayout.Controls.Add(this.bonusLifesteal, 2, 3);
            this.attackDamageStatsTableLayout.Controls.Add(this.bonusCriticalChance, 2, 2);
            this.attackDamageStatsTableLayout.Controls.Add(this.bonusAttackSpeed, 2, 1);
            this.attackDamageStatsTableLayout.Controls.Add(this.attackDamageLabel, 0, 0);
            this.attackDamageStatsTableLayout.Controls.Add(this.attackSpeedLabel, 0, 1);
            this.attackDamageStatsTableLayout.Controls.Add(this.criticalChanceLabel, 0, 2);
            this.attackDamageStatsTableLayout.Controls.Add(this.lifestealLabel, 0, 3);
            this.attackDamageStatsTableLayout.Controls.Add(this.attackDamage, 1, 0);
            this.attackDamageStatsTableLayout.Controls.Add(this.attackSpeed, 1, 1);
            this.attackDamageStatsTableLayout.Controls.Add(this.criticalChance, 1, 2);
            this.attackDamageStatsTableLayout.Controls.Add(this.lifesteal, 1, 3);
            this.attackDamageStatsTableLayout.Controls.Add(this.bonusAttackDamage, 2, 0);
            this.attackDamageStatsTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attackDamageStatsTableLayout.Location = new System.Drawing.Point(3, 28);
            this.attackDamageStatsTableLayout.Name = "attackDamageStatsTableLayout";
            this.attackDamageStatsTableLayout.RowCount = 5;
            this.attackDamageStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.attackDamageStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.attackDamageStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.attackDamageStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.attackDamageStatsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.attackDamageStatsTableLayout.Size = new System.Drawing.Size(238, 78);
            this.attackDamageStatsTableLayout.TabIndex = 1;
            // 
            // bonusLifesteal
            // 
            this.bonusLifesteal.AutoSize = true;
            this.bonusLifesteal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bonusLifesteal.ForeColor = System.Drawing.Color.LightCoral;
            this.bonusLifesteal.Location = new System.Drawing.Point(177, 60);
            this.bonusLifesteal.Name = "bonusLifesteal";
            this.bonusLifesteal.Size = new System.Drawing.Size(58, 20);
            this.bonusLifesteal.TabIndex = 11;
            this.bonusLifesteal.Text = "bls";
            this.bonusLifesteal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bonusCriticalChance
            // 
            this.bonusCriticalChance.AutoSize = true;
            this.bonusCriticalChance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bonusCriticalChance.ForeColor = System.Drawing.Color.DarkRed;
            this.bonusCriticalChance.Location = new System.Drawing.Point(177, 40);
            this.bonusCriticalChance.Name = "bonusCriticalChance";
            this.bonusCriticalChance.Size = new System.Drawing.Size(58, 20);
            this.bonusCriticalChance.TabIndex = 10;
            this.bonusCriticalChance.Text = "bcc";
            this.bonusCriticalChance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bonusAttackSpeed
            // 
            this.bonusAttackSpeed.AutoSize = true;
            this.bonusAttackSpeed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bonusAttackSpeed.ForeColor = System.Drawing.Color.Firebrick;
            this.bonusAttackSpeed.Location = new System.Drawing.Point(177, 20);
            this.bonusAttackSpeed.Name = "bonusAttackSpeed";
            this.bonusAttackSpeed.Size = new System.Drawing.Size(58, 20);
            this.bonusAttackSpeed.TabIndex = 9;
            this.bonusAttackSpeed.Text = "bas";
            this.bonusAttackSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // attackDamageLabel
            // 
            this.attackDamageLabel.AutoSize = true;
            this.attackDamageLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attackDamageLabel.Location = new System.Drawing.Point(3, 0);
            this.attackDamageLabel.Name = "attackDamageLabel";
            this.attackDamageLabel.Size = new System.Drawing.Size(104, 20);
            this.attackDamageLabel.TabIndex = 0;
            this.attackDamageLabel.Text = "Attack damage :";
            this.attackDamageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // attackSpeedLabel
            // 
            this.attackSpeedLabel.AutoSize = true;
            this.attackSpeedLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attackSpeedLabel.Location = new System.Drawing.Point(3, 20);
            this.attackSpeedLabel.Name = "attackSpeedLabel";
            this.attackSpeedLabel.Size = new System.Drawing.Size(104, 20);
            this.attackSpeedLabel.TabIndex = 1;
            this.attackSpeedLabel.Text = "Attack speed :";
            this.attackSpeedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // criticalChanceLabel
            // 
            this.criticalChanceLabel.AutoSize = true;
            this.criticalChanceLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.criticalChanceLabel.Location = new System.Drawing.Point(3, 40);
            this.criticalChanceLabel.Name = "criticalChanceLabel";
            this.criticalChanceLabel.Size = new System.Drawing.Size(104, 20);
            this.criticalChanceLabel.TabIndex = 2;
            this.criticalChanceLabel.Text = "Critical chance :";
            this.criticalChanceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lifestealLabel
            // 
            this.lifestealLabel.AutoSize = true;
            this.lifestealLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lifestealLabel.Location = new System.Drawing.Point(3, 60);
            this.lifestealLabel.Name = "lifestealLabel";
            this.lifestealLabel.Size = new System.Drawing.Size(104, 20);
            this.lifestealLabel.TabIndex = 3;
            this.lifestealLabel.Text = "Lifesteal :";
            this.lifestealLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // attackDamage
            // 
            this.attackDamage.AutoSize = true;
            this.attackDamage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attackDamage.Location = new System.Drawing.Point(113, 0);
            this.attackDamage.Name = "attackDamage";
            this.attackDamage.Size = new System.Drawing.Size(58, 20);
            this.attackDamage.TabIndex = 4;
            this.attackDamage.Text = "ad";
            this.attackDamage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // attackSpeed
            // 
            this.attackSpeed.AutoSize = true;
            this.attackSpeed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attackSpeed.Location = new System.Drawing.Point(113, 20);
            this.attackSpeed.Name = "attackSpeed";
            this.attackSpeed.Size = new System.Drawing.Size(58, 20);
            this.attackSpeed.TabIndex = 5;
            this.attackSpeed.Text = "as";
            this.attackSpeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // criticalChance
            // 
            this.criticalChance.AutoSize = true;
            this.criticalChance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.criticalChance.Location = new System.Drawing.Point(113, 40);
            this.criticalChance.Name = "criticalChance";
            this.criticalChance.Size = new System.Drawing.Size(58, 20);
            this.criticalChance.TabIndex = 6;
            this.criticalChance.Text = "cc";
            this.criticalChance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lifesteal
            // 
            this.lifesteal.AutoSize = true;
            this.lifesteal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lifesteal.Location = new System.Drawing.Point(113, 60);
            this.lifesteal.Name = "lifesteal";
            this.lifesteal.Size = new System.Drawing.Size(58, 20);
            this.lifesteal.TabIndex = 7;
            this.lifesteal.Text = "ls";
            this.lifesteal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bonusAttackDamage
            // 
            this.bonusAttackDamage.AutoSize = true;
            this.bonusAttackDamage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bonusAttackDamage.ForeColor = System.Drawing.Color.Red;
            this.bonusAttackDamage.Location = new System.Drawing.Point(177, 0);
            this.bonusAttackDamage.Name = "bonusAttackDamage";
            this.bonusAttackDamage.Size = new System.Drawing.Size(58, 20);
            this.bonusAttackDamage.TabIndex = 8;
            this.bonusAttackDamage.Text = "bad";
            this.bonusAttackDamage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // itemTableLayout
            // 
            this.itemTableLayout.ColumnCount = 1;
            this.itemTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.itemTableLayout.Controls.Add(this.buildTableLayout, 0, 0);
            this.itemTableLayout.Controls.Add(this.researchTableLayout, 0, 1);
            this.itemTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.itemTableLayout.Location = new System.Drawing.Point(500, 0);
            this.itemTableLayout.Margin = new System.Windows.Forms.Padding(0);
            this.itemTableLayout.Name = "itemTableLayout";
            this.itemTableLayout.RowCount = 2;
            this.itemTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.itemTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.itemTableLayout.Size = new System.Drawing.Size(325, 386);
            this.itemTableLayout.TabIndex = 1;
            // 
            // buildTableLayout
            // 
            this.buildTableLayout.ColumnCount = 2;
            this.buildTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 195F));
            this.buildTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.buildTableLayout.Controls.Add(this.buildCaseTableLayout, 0, 0);
            this.buildTableLayout.Controls.Add(this.buildActionsTableLayout, 1, 0);
            this.buildTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buildTableLayout.Location = new System.Drawing.Point(0, 0);
            this.buildTableLayout.Margin = new System.Windows.Forms.Padding(0);
            this.buildTableLayout.Name = "buildTableLayout";
            this.buildTableLayout.RowCount = 1;
            this.buildTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.buildTableLayout.Size = new System.Drawing.Size(325, 130);
            this.buildTableLayout.TabIndex = 1;
            // 
            // buildCaseTableLayout
            // 
            this.buildCaseTableLayout.ColumnCount = 3;
            this.buildCaseTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.buildCaseTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.buildCaseTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.buildCaseTableLayout.Controls.Add(this.item4PictureBox, 0, 1);
            this.buildCaseTableLayout.Controls.Add(this.item5PictureBox, 0, 1);
            this.buildCaseTableLayout.Controls.Add(this.item6PictureBox, 0, 1);
            this.buildCaseTableLayout.Controls.Add(this.item1PictureBox, 0, 0);
            this.buildCaseTableLayout.Controls.Add(this.item2PictureBox, 1, 0);
            this.buildCaseTableLayout.Controls.Add(this.item3PictureBox, 2, 0);
            this.buildCaseTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buildCaseTableLayout.Location = new System.Drawing.Point(0, 0);
            this.buildCaseTableLayout.Margin = new System.Windows.Forms.Padding(0);
            this.buildCaseTableLayout.Name = "buildCaseTableLayout";
            this.buildCaseTableLayout.RowCount = 2;
            this.buildCaseTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.buildCaseTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.buildCaseTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.buildCaseTableLayout.Size = new System.Drawing.Size(195, 130);
            this.buildCaseTableLayout.TabIndex = 0;
            // 
            // item4PictureBox
            // 
            this.item4PictureBox.BackColor = System.Drawing.Color.Black;
            this.item4PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.item4PictureBox.Location = new System.Drawing.Point(3, 68);
            this.item4PictureBox.Name = "item4PictureBox";
            this.item4PictureBox.Size = new System.Drawing.Size(59, 59);
            this.item4PictureBox.TabIndex = 3;
            this.item4PictureBox.TabStop = false;
            this.item4PictureBox.Click += new System.EventHandler(this.OnItem4PictureBoxClick);
            // 
            // item5PictureBox
            // 
            this.item5PictureBox.BackColor = System.Drawing.Color.Black;
            this.item5PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.item5PictureBox.Location = new System.Drawing.Point(68, 68);
            this.item5PictureBox.Name = "item5PictureBox";
            this.item5PictureBox.Size = new System.Drawing.Size(59, 59);
            this.item5PictureBox.TabIndex = 2;
            this.item5PictureBox.TabStop = false;
            this.item5PictureBox.Click += new System.EventHandler(this.OnItem5PictureBoxClick);
            // 
            // item6PictureBox
            // 
            this.item6PictureBox.BackColor = System.Drawing.Color.Black;
            this.item6PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.item6PictureBox.Location = new System.Drawing.Point(133, 68);
            this.item6PictureBox.Name = "item6PictureBox";
            this.item6PictureBox.Size = new System.Drawing.Size(59, 59);
            this.item6PictureBox.TabIndex = 1;
            this.item6PictureBox.TabStop = false;
            this.item6PictureBox.Click += new System.EventHandler(this.OnItem6PictureBoxClick);
            // 
            // item1PictureBox
            // 
            this.item1PictureBox.BackColor = System.Drawing.Color.Black;
            this.item1PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.item1PictureBox.Location = new System.Drawing.Point(3, 3);
            this.item1PictureBox.Name = "item1PictureBox";
            this.item1PictureBox.Size = new System.Drawing.Size(59, 59);
            this.item1PictureBox.TabIndex = 0;
            this.item1PictureBox.TabStop = false;
            this.item1PictureBox.Click += new System.EventHandler(this.OnItem1PictureBoxClick);
            // 
            // item2PictureBox
            // 
            this.item2PictureBox.BackColor = System.Drawing.Color.Black;
            this.item2PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.item2PictureBox.Location = new System.Drawing.Point(68, 3);
            this.item2PictureBox.Name = "item2PictureBox";
            this.item2PictureBox.Size = new System.Drawing.Size(59, 59);
            this.item2PictureBox.TabIndex = 4;
            this.item2PictureBox.TabStop = false;
            this.item2PictureBox.Click += new System.EventHandler(this.OnItem2PictureBoxClick);
            // 
            // item3PictureBox
            // 
            this.item3PictureBox.BackColor = System.Drawing.Color.Black;
            this.item3PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.item3PictureBox.Location = new System.Drawing.Point(133, 3);
            this.item3PictureBox.Name = "item3PictureBox";
            this.item3PictureBox.Size = new System.Drawing.Size(59, 59);
            this.item3PictureBox.TabIndex = 5;
            this.item3PictureBox.TabStop = false;
            this.item3PictureBox.Click += new System.EventHandler(this.OnItem3PictureBoxClick);
            // 
            // buildActionsTableLayout
            // 
            this.buildActionsTableLayout.ColumnCount = 1;
            this.buildActionsTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.buildActionsTableLayout.Controls.Add(this.price, 0, 0);
            this.buildActionsTableLayout.Controls.Add(this.resetButton, 0, 2);
            this.buildActionsTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buildActionsTableLayout.Location = new System.Drawing.Point(198, 3);
            this.buildActionsTableLayout.Name = "buildActionsTableLayout";
            this.buildActionsTableLayout.RowCount = 3;
            this.buildActionsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.buildActionsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.buildActionsTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.buildActionsTableLayout.Size = new System.Drawing.Size(124, 124);
            this.buildActionsTableLayout.TabIndex = 1;
            // 
            // price
            // 
            this.price.AutoSize = true;
            this.price.Dock = System.Windows.Forms.DockStyle.Fill;
            this.price.Location = new System.Drawing.Point(3, 0);
            this.price.Name = "price";
            this.price.Size = new System.Drawing.Size(118, 41);
            this.price.TabIndex = 0;
            this.price.Text = "price";
            this.price.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // resetButton
            // 
            this.resetButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resetButton.Location = new System.Drawing.Point(3, 85);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(118, 36);
            this.resetButton.TabIndex = 2;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.OnResetButtonClick);
            // 
            // researchTableLayout
            // 
            this.researchTableLayout.ColumnCount = 1;
            this.researchTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.researchTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.researchTableLayout.Controls.Add(this.searchField, 0, 0);
            this.researchTableLayout.Controls.Add(this.itemListView, 0, 1);
            this.researchTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.researchTableLayout.Location = new System.Drawing.Point(3, 133);
            this.researchTableLayout.Name = "researchTableLayout";
            this.researchTableLayout.RowCount = 2;
            this.researchTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.researchTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.researchTableLayout.Size = new System.Drawing.Size(319, 250);
            this.researchTableLayout.TabIndex = 0;
            // 
            // searchField
            // 
            this.searchField.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchField.Location = new System.Drawing.Point(3, 3);
            this.searchField.Name = "searchField";
            this.searchField.Size = new System.Drawing.Size(313, 20);
            this.searchField.TabIndex = 0;
            this.searchField.TextChanged += new System.EventHandler(this.OnSearchFieldTextChanged);
            // 
            // itemListView
            // 
            this.itemListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.itemListView.Location = new System.Drawing.Point(3, 28);
            this.itemListView.Name = "itemListView";
            this.itemListView.Size = new System.Drawing.Size(313, 219);
            this.itemListView.TabIndex = 1;
            this.itemListView.UseCompatibleStateImageBehavior = false;
            this.itemListView.ItemActivate += new System.EventHandler(this.OnItemListViewItemActivate);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 386);
            this.Controls.Add(this.mainTableLayout);
            this.MinimumSize = new System.Drawing.Size(841, 425);
            this.Name = "MainWindow";
            this.Text = "League of Stats";
            this.mainTableLayout.ResumeLayout(false);
            this.championTableLayout.ResumeLayout(false);
            this.championHeaderTableLayout.ResumeLayout(false);
            this.namePictureTableLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.image)).EndInit();
            this.titleLoreTableLayout.ResumeLayout(false);
            this.titleLoreTableLayout.PerformLayout();
            this.statsTableLayout.ResumeLayout(false);
            this.rightStatsTableLayout.ResumeLayout(false);
            this.defenseUtilityTableLayout.ResumeLayout(false);
            this.defenseUtilityTableLayout.PerformLayout();
            this.defenseUtilityStatsTableLayout.ResumeLayout(false);
            this.defenseUtilityStatsTableLayout.PerformLayout();
            this.abilityPowerTableLayout.ResumeLayout(false);
            this.abilityPowerTableLayout.PerformLayout();
            this.abilityPowerStatsTableLayout.ResumeLayout(false);
            this.abilityPowerStatsTableLayout.PerformLayout();
            this.leftStatsTableLayout.ResumeLayout(false);
            this.healthManaTableLayout.ResumeLayout(false);
            this.healthManaTableLayout.PerformLayout();
            this.healthManaStatsTableLayout.ResumeLayout(false);
            this.healthManaStatsTableLayout.PerformLayout();
            this.attackDamageTableLayout.ResumeLayout(false);
            this.attackDamageTableLayout.PerformLayout();
            this.attackDamageStatsTableLayout.ResumeLayout(false);
            this.attackDamageStatsTableLayout.PerformLayout();
            this.itemTableLayout.ResumeLayout(false);
            this.buildTableLayout.ResumeLayout(false);
            this.buildCaseTableLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.item4PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item5PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item6PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item2PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item3PictureBox)).EndInit();
            this.buildActionsTableLayout.ResumeLayout(false);
            this.buildActionsTableLayout.PerformLayout();
            this.researchTableLayout.ResumeLayout(false);
            this.researchTableLayout.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainTableLayout;
        private System.Windows.Forms.TableLayoutPanel championTableLayout;
        private System.Windows.Forms.TableLayoutPanel championHeaderTableLayout;
        private System.Windows.Forms.TableLayoutPanel namePictureTableLayout;
        private System.Windows.Forms.PictureBox image;
        private System.Windows.Forms.ComboBox nameComboBox;
        private System.Windows.Forms.TableLayoutPanel titleLoreTableLayout;
        private System.Windows.Forms.Label title;
        private System.Windows.Forms.TableLayoutPanel statsTableLayout;
        private System.Windows.Forms.TableLayoutPanel leftStatsTableLayout;
        private System.Windows.Forms.TableLayoutPanel healthManaTableLayout;
        private System.Windows.Forms.Label healthManaTitle;
        private System.Windows.Forms.TableLayoutPanel healthManaStatsTableLayout;
        private System.Windows.Forms.Label healthLabel;
        private System.Windows.Forms.Label healthRegenLabel;
        private System.Windows.Forms.Label manaLabel;
        private System.Windows.Forms.Label manaRegenLabel;
        private System.Windows.Forms.Label health;
        private System.Windows.Forms.Label healthRegen;
        private System.Windows.Forms.Label mana;
        private System.Windows.Forms.Label manaRegen;
        private System.Windows.Forms.TableLayoutPanel defenseUtilityTableLayout;
        private System.Windows.Forms.TableLayoutPanel defenseUtilityStatsTableLayout;
        private System.Windows.Forms.Label armorLabel;
        private System.Windows.Forms.Label magicResistLabel;
        private System.Windows.Forms.Label movementSpeedLabel;
        private System.Windows.Forms.Label armor;
        private System.Windows.Forms.Label magicResist;
        private System.Windows.Forms.Label movementSpeed;
        private System.Windows.Forms.Label defenseUtilityTitle;
        private System.Windows.Forms.TableLayoutPanel rightStatsTableLayout;
        private System.Windows.Forms.TableLayoutPanel attackDamageTableLayout;
        private System.Windows.Forms.Label attackDamageTitle;
        private System.Windows.Forms.TableLayoutPanel attackDamageStatsTableLayout;
        private System.Windows.Forms.Label attackDamageLabel;
        private System.Windows.Forms.Label attackSpeedLabel;
        private System.Windows.Forms.Label criticalChanceLabel;
        private System.Windows.Forms.Label lifestealLabel;
        private System.Windows.Forms.Label attackDamage;
        private System.Windows.Forms.Label attackSpeed;
        private System.Windows.Forms.Label criticalChance;
        private System.Windows.Forms.Label lifesteal;
        private System.Windows.Forms.TableLayoutPanel abilityPowerTableLayout;
        private System.Windows.Forms.TableLayoutPanel abilityPowerStatsTableLayout;
        private System.Windows.Forms.Label abilityPowerLabel;
        private System.Windows.Forms.Label coolddownReductionLabel;
        private System.Windows.Forms.Label spellVampLabel;
        private System.Windows.Forms.Label abilityPower;
        private System.Windows.Forms.Label cooldownReduction;
        private System.Windows.Forms.Label spellvamp;
        private System.Windows.Forms.Label abilityPowerTitle;
        private System.Windows.Forms.RichTextBox loreBox;
        private System.Windows.Forms.TableLayoutPanel itemTableLayout;
        private System.Windows.Forms.TableLayoutPanel buildTableLayout;
        private System.Windows.Forms.TableLayoutPanel researchTableLayout;
        private System.Windows.Forms.TextBox searchField;
        private System.Windows.Forms.ListView itemListView;
        private System.Windows.Forms.TableLayoutPanel buildCaseTableLayout;
        private System.Windows.Forms.PictureBox item1PictureBox;
        private System.Windows.Forms.TableLayoutPanel buildActionsTableLayout;
        private System.Windows.Forms.Label price;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.PictureBox item4PictureBox;
        private System.Windows.Forms.PictureBox item5PictureBox;
        private System.Windows.Forms.PictureBox item6PictureBox;
        private System.Windows.Forms.PictureBox item2PictureBox;
        private System.Windows.Forms.PictureBox item3PictureBox;
        private System.Windows.Forms.Label bonusManaRegen;
        private System.Windows.Forms.Label bonusMana;
        private System.Windows.Forms.Label bonusHealthRegen;
        private System.Windows.Forms.Label bonusHealth;
        private System.Windows.Forms.Label bonusLifesteal;
        private System.Windows.Forms.Label bonusCriticalChance;
        private System.Windows.Forms.Label bonusAttackSpeed;
        private System.Windows.Forms.Label bonusAttackDamage;
        private System.Windows.Forms.Label bonusMovementSpeed;
        private System.Windows.Forms.Label bonusMagicResist;
        private System.Windows.Forms.Label bonusArmor;
        private System.Windows.Forms.Label bonusSpellvamp;
        private System.Windows.Forms.Label bonusCooldownReduction;
        private System.Windows.Forms.Label bonusAbilityPower;
    }
}

