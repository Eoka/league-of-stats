﻿using System;
using System.Collections.Generic;

namespace League_of_Stats.Views.Interfaces
{
    public class NameArgs : EventArgs
    {
        public string Name { get; set; }
    }

    public interface IMainWindow
    {
        event EventHandler<NameArgs> selectedChampionChanged;
        event EventHandler searchChanged;
        event EventHandler<NameArgs> itemInListClicked;
        event EventHandler resetButtonClicked;
        event EventHandler<NameArgs> item1Clicked;
        event EventHandler<NameArgs> item2Clicked;
        event EventHandler<NameArgs> item3Clicked;
        event EventHandler<NameArgs> item4Clicked;
        event EventHandler<NameArgs> item5Clicked;
        event EventHandler<NameArgs> item6Clicked;

        IList<string> ChampionsName { set; }
        IList<string> ItemsName { set; }

        string SearchText { get; }

        string ChampionImageURL { set; }
        string Title { set; }
        string Lore { set; }

        string Health { set; }
        string HealthRegen { set; }
        string Mana { set; }
        string ManaRegen { set; }
        string Armor { set; }
        string MagicResist { set; }
        string MovementSpeed { set; }
        string AttackDamage { set; }
        string AttackSpeed { set; }
        string CriticalChance { set; }
        string Lifesteal { set; }
        string AbilityPower { set; }
        string CooldownReduction { set; }
        string Spellvamp { set; }

        string BonusHealth { set; }
        string BonusHealthRegen { set; }
        string BonusMana { set; }
        string BonusManaRegen { set; }
        string BonusArmor { set; }
        string BonusMagicResist { set; }
        string BonusMovementSpeed { set; }
        string BonusAttackDamage { set; }
        string BonusAttackSpeed { set; }
        string BonusCriticalChance { set; }
        string BonusLifesteal { set; }
        string BonusAbilityPower { set; }
        string BonusCooldownReduction { set; }
        string BonusSpellvamp { set; }

        string Price { set; }

        string Item1Image { set; }
        string Item2Image { set; }
        string Item3Image { set; }
        string Item4Image { set; }
        string Item5Image { set; }
        string Item6Image { set; }

        void ShowError(string text);
    }
}
