﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RiotApi.Net.RestClient.Dto.LolStaticData.Item;

namespace League_of_Stats.Models
{
    public class Stats
    {
        public double Health              { get; set; }
        public double HealthRegen         { get; set; }
        public double Mana                { get; set; }
        public double ManaRegen           { get; set; }
        public double Armor               { get; set; }
        public double MagicResist         { get; set; }
        public double MovementSpeed       { get; set; }
        public double AttackDamage        { get; set; }
        public double AttackSpeed         { get; set; }
        public double CriticalChance      { get; set; }
        public double Lifesteal           { get; set; }
        public double AbilityPower        { get; set; }
        public double CooldownReduction   { get; set; }
        public double Spellvamp           { get; set; }
        
        public static Stats operator +(Stats a, Stats b)
        {
            return new Stats()
            {
                Health = a.Health + b.Health,
                HealthRegen = a.HealthRegen + b.HealthRegen,
                Mana = a.Mana + b.Mana,
                ManaRegen = a.ManaRegen + b.ManaRegen,
                Armor = a.Armor + b.Armor,
                MagicResist = a.MagicResist + b.MagicResist,
                MovementSpeed = a.MovementSpeed + b.MovementSpeed,
                AttackDamage = a.AttackDamage + b.AttackDamage,
                AttackSpeed = a.AttackSpeed + b.AttackSpeed,
                CriticalChance = a.CriticalChance + b.CriticalChance,
                Lifesteal = a.Lifesteal + b.Lifesteal,
                AbilityPower = a.AbilityPower + b.AbilityPower,
                CooldownReduction = a.CooldownReduction + b.CooldownReduction,
                Spellvamp = a.Spellvamp + b.Spellvamp
            };
        }

    }
}
