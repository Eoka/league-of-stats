﻿using RiotApi.Net.RestClient;
using System.Collections.Generic;
using System.Linq;
using static RiotApi.Net.RestClient.Configuration.RiotApiConfig;
using RiotApi.Net.RestClient.Dto.LolStaticData.Champion;
using RiotApi.Net.RestClient.Dto.LolStaticData.Item;
using System;

namespace League_of_Stats.Models
{
    public class DataController
    {
        private readonly string DDRAGONIMG;

        private IRiotClient client;
        private Regions currentRegion;
        private string currentVersion;
        private Dictionary<string, ChampionDto> championsDto;
        private Dictionary<string, ItemDto> itemsDto;
        private List<string> championsName;
        private List<string> itemsName;

        public IList<string> ChampionsName { get { return championsName; } }
        public IList<string> ItemsName { get { return itemsName; } }

        public DataController()
        {
            try
            {
                client = new RiotClient("23539b62-8140-42b8-bb2a-fac5beefa306");
                currentRegion = Regions.EUW;
                currentVersion = client.LolStaticData.GetVersionData(currentRegion).First();
                InitChampions();
                InitItems();

                DDRAGONIMG = string.Format("http://ddragon.leagueoflegends.com/cdn/{0}/img/", currentVersion);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void InitChampions()
        {
            championsDto = client.LolStaticData.GetChampionList(currentRegion, null, null, currentVersion, "all").Data;

            championsName = new List<string>();
            foreach (KeyValuePair<string, ChampionDto> champion in championsDto)
                championsName.Add(champion.Value.Name);

            championsName.Sort();
        }

        private void InitItems()
        {
            itemsDto = client.LolStaticData.GetItemList(currentRegion, null, currentVersion, "all").Data;

            itemsName = new List<string>();
            foreach (KeyValuePair<string, ItemDto> item in itemsDto)
                itemsName.Add(item.Value.Name);

            itemsName.Sort();
        }

        public KeyValuePair<string, ChampionDto> GetChampionDictionary(string name)
        {
            foreach (KeyValuePair<string, ChampionDto> champion in championsDto)
                if (champion.Value.Name.Equals(name))
                    return champion;

            return new KeyValuePair<string, ChampionDto>();
        }

        public ChampionDto GetChampionDto(string name)
        {
            return GetChampionDictionary(name).Value;
        }

        public string GetChampionImageURL(string imageFull)
        {
            return GetImage("champion", imageFull);
        }

        private KeyValuePair<string, ItemDto> GetItemDictionary(string name)
        {
            foreach (KeyValuePair<string, ItemDto> item in itemsDto)
                if (item.Value.Name.Equals(name))
                    return item;

            return new KeyValuePair<string, ItemDto>();
        }

        public ItemDto GetItemDto(string name)
        {
            return GetItemDictionary(name).Value;
        }

        public string GetItemImageUrl(string imageFull)
        {
            return GetImage("item", imageFull);
        }

        private string GetImage(string type, string imageFull)
        {
            return string.Format("{0}{1}/{2}", DDRAGONIMG, type, imageFull);
        }
    }
}
