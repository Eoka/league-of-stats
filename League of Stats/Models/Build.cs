﻿using RiotApi.Net.RestClient.Dto.LolStaticData.Item;

namespace League_of_Stats.Models
{
    public class Build
    {
        private ItemDto[] build;
        private int firstEmptySpace;

        public int Price
        {
            get
            {
                int price = 0;
                for (int i = 0; i < build.Length; i++)
                    if (build[i] != null)
                        price += build[i].Gold.Total;
                return price;
            }
        }

        public ItemDto this[int i]
        {
            get { return build[i]; }
        }

        public Build()
        {
            build = new ItemDto[6];
            firstEmptySpace = 0;
        }

        public void AddItemToBuild(ItemDto item)
        {
            if (firstEmptySpace >= 0)
                build[firstEmptySpace] = item;
            GetFirstEmptySpace();
        }

        public void Reset()
        {
            for (int i = 0; i < build.Length; i++)
                RemoveItemAtIndex(i);
            GetFirstEmptySpace();
        }

        public void RemoveItemAtIndex(int index)
        {
            build[index] = null;
            GetFirstEmptySpace();
        }

        private void GetFirstEmptySpace()
        {
            firstEmptySpace = -1;

            for (int i = 0; i < build.Length; i++)
                if (build[i] == null)
                {
                    firstEmptySpace = i;
                    break;
                }
        }

        public Stats GetBuildStats()
        {
            Stats stats = new Stats();
            foreach (ItemDto item in build)
                if (item != null)
                    stats += new Stats()
                    {
                        Health = item.Stats.FlatHPPoolMod,
                        HealthRegen = item.Stats.PercentHPRegenMod,
                        Mana = item.Stats.FlatMPPoolMod,
                        ManaRegen = item.Stats.PercentMPRegenMod,
                        Armor = item.Stats.FlatArmorMod,
                        MagicResist = item.Stats.FlatSpellBlockMod,
                        MovementSpeed = item.Stats.PercentMovementSpeedMod,
                        AttackDamage = item.Stats.FlatPhysicalDamageMod,
                        AttackSpeed = item.Stats.PercentAttackSpeedMod,
                        CriticalChance = item.Stats.FlatCritChanceMod,
                        Lifesteal = item.Stats.PercentLifeStealMod,
                        AbilityPower = item.Stats.FlatMagicDamageMod,
                        CooldownReduction = item.Stats.RPercentCooldownMod,
                        Spellvamp = item.Stats.PercentSpellVampMod
                    };

            return stats;
        }
    }
}
